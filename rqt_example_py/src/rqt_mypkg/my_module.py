# importing main ros packages
import rospy
import rospkg
import tf.transformations
import numpy as np
import os
import time
from geometry_msgs.msg import PoseStamped
from franka_msgs.msg import FrankaState
import roslaunch

# importing ui packages
from qt_gui.plugin import Plugin
from python_qt_binding.QtWidgets import *
from python_qt_binding.QtGui import *
from python_qt_binding.QtCore import *


# Main Plugin class
class MyPlugin(Plugin):
    def __init__(self, context):
        super(MyPlugin, self).__init__(context)

        # Give QObjects reasonable names
        self.setObjectName('MyPlugin')
        rp = rospkg.RosPack()

        self.marker_pose = PoseStamped()
        self.state_sub = rospy.Subscriber("franka_state_controller/franka_states",
                                          FrankaState, self.franka_state_callback)
        self.pose_pub = rospy.Publisher("equilibrium_pose", PoseStamped, queue_size=10)
        self.link_name = "panda_link0"
        self.listener = tf.TransformListener()
        self.count = 0
        # Create QWidget
        self.widget = QWidget()
        context.add_widget(self.widget)

        # Layout and attach to widget
        layout = QVBoxLayout()
        self.widget.setLayout(layout)

        self.grid_layout = QGridLayout()
        layout.addLayout(self.grid_layout)
        self.trajectory = []
        self._teach_button = QRadioButton("Teach")
        self._teach_button.setChecked(False)
        self.grid_layout.addWidget(self._teach_button, 1, 4)

        self._imitate_button = QRadioButton("Imitate")
        self._imitate_button.setChecked(False)
        self.grid_layout.addWidget(self._imitate_button, 2, 4)

        self._null_button = QRadioButton("Null")
        self._imitate_button.setChecked(False)
        self.grid_layout.addWidget(self._null_button, 3, 4)

        img_file = os.path.join(rp.get_path('rqt_mypkg'), 'resource', 'TurtleBot.jpg')
        self._pic_label = QLabel()
        self._pixmap = QPixmap(img_file)
        self._pixmap = self._pixmap.scaled(256, 192)
        self._pic_label.setPixmap(self._pixmap)
        # self.label.resize(100, 50)
        self.grid_layout.addWidget(self._pic_label, 2, 0, 3, 1)

        self._font = QFont()
        self._font.setBold(True)
        self._font.setPointSize(20)
        self._head_label = QLabel()
        self._head_label.setFont(self._font)
        self._head_label.setText('LfD')
        self.grid_layout.addWidget(self._head_label, 0, 0, 1, 1)

    def franka_state_callback(self, msg):
        if self._teach_button.isChecked():
            initial_quaternion = \
                tf.transformations.quaternion_from_matrix(
                    np.transpose(np.reshape(msg.O_T_EE,
                                            (4, 4))))
            initial_quaternion = initial_quaternion / np.linalg.norm(initial_quaternion)
            orientation_x = initial_quaternion[0]
            orientation_y = initial_quaternion[1]
            orientation_z = initial_quaternion[2]
            orientation_w = initial_quaternion[3]
            self.trajectory.append([msg.O_T_EE[12], msg.O_T_EE[13], msg.O_T_EE[14],
                                    orientation_x, orientation_y, orientation_z, orientation_w])
            print msg.O_T_EE[12]
        elif self._imitate_button.isChecked():
            if self.count < len(self.trajectory):
                print(self.count)
                self.marker_pose.header.frame_id = self.link_name
                self.marker_pose.header.stamp = rospy.Time(0)
                self.marker_pose.pose.position.x = self.trajectory[self.count][0]
                self.marker_pose.pose.position.y = self.trajectory[self.count][1]
                self.marker_pose.pose.position.z = self.trajectory[self.count][2]

                self.marker_pose.pose.orientation.x = self.trajectory[self.count][3]
                self.marker_pose.pose.orientation.y = self.trajectory[self.count][4]
                self.marker_pose.pose.orientation.z = self.trajectory[self.count][5]
                self.marker_pose.pose.orientation.w = self.trajectory[self.count][6]

                time.sleep(0.1)
                self.pose_pub.publish(self.marker_pose)
                self.count += 1

    def shutdown_plugin(self):
        # TODO unregister all publishers here
        self._timeline.handle_close()
        pass

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

        # def trigger_configuration(self):
        # Comment in to signal that the plugin has a way to configure
        # This will enable a setting button (gear icon) in each dock widget title bar
        # Usually used to open a modal configuration dialog
