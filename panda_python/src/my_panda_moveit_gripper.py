import sys
# import roscpp
import rospy
import moveit_commander
import moveit_msgs.msg
from geometry_msgs.msg import PoseStamped
# import gcode_reader


moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('moveit_py_demo', anonymous=True)

# create objects for using moveit_commander
robot = moveit_commander.RobotCommander()

scene = moveit_commander.PlanningSceneInterface()
# print(robot.get_group_names())
group = moveit_commander.MoveGroupCommander("hand")
# print(group.get_name())
rospy.sleep(1)
# clean the scene
scene.remove_world_object("pole")
scene.remove_world_object("table")
scene.remove_world_object("part")

# publish a demo scene
p = PoseStamped()
p.header.frame_id = robot.get_planning_frame()
p.pose.position.x = 0.7
p.pose.position.y = -0.4
p.pose.position.z = 0.85
p.pose.orientation.w = 1.0
scene.add_box("pole", p, (0.3, 0.1, 1.0))

p.pose.position.y = -0.2
p.pose.position.z = 0.175
scene.add_box("table", p, (0.5, 1.5, 0.35))

p.pose.position.x = 0.6
p.pose.position.y = -0.7
p.pose.position.z = 0.5
scene.add_box("part", p, (0.15, 0.1, 0.3))

robot.get_group('hand').pick("part")

rospy.spin()
moveit_commander.roscpp_shutdown()
