Packages
========

franka_control
--------------

``franka_control`` provides a variety of ROS services to expose the full ``libfranka``
API in the ROS ecosystem. For detailed documentation of ``franka_control`` visit `franka_documentation <https://frankaemika.github.io/docs/ros_introduction.html>`_.

To recover from errors and reflexes the ``franka_control::ErrorRecoveryAction`` can be called.
That can be done from an action client or by simply publishing on the action goal topic:

.. code-block:: shell

   rostopic pub -1 /franka_control/error_recovery/goal franka_control/ErrorRecoveryActionGoal "{}"


After recovery, the ``franka_control_node`` restarts the controllers that were running. That is
possible as the node does not die when robot reflexes are triggered or errors are occurred.
All of these functionalities are provided by the ``franka_control_node`` which can be launched
with the following command:

.. code-block:: shell

    roslaunch franka_control franka_control.launch robot_ip:=<fci-ip> load_gripper:=<true|false>

franka_example_controllers
--------------------------
In this package a set of example controllers for controlling the robot via ROS are implemented.
The controllers depict the variety of interfaces offered by the ``franka_hw::FrankaHW`` class and
the according usage. Each example comes with a separate stand-alone launch file that starts the
controller on the robot and visualizes it.

To launch the joint impedance example, execute the following command:

.. code-block:: shell

    roslaunch franka_example_controllers joint_impedance_example_controller.launch \
      robot_ip:=<fci-ip> load_gripper:=<true|false>

Other examples are started in the same way.

panda_moveit_config
--------------------
This package contains partly auto generated files that provide an out-of-the-box MoveIt!
configuration for Panda Arm and Hand.

To control the robot with MoveIt! launch the following three files:

.. code-block:: shell

    # Bring up the controller manager and connect to the robot
    roslaunch franka_control franka_control.launch robot_ip:=<fci-ip> load_gripper:=<true|false>

    # (Optional) If load_gripper:=true was used, start the gripper node as well
    roslaunch franka_gripper franka_gripper.launch robot_ip:=<fci-ip>

    # Start a joint trajectory controller of type <controller>
    roslaunch panda_moveit_config panda_moveit.launch controller:=<effort/position> \
      load_gripper:=<true|false>

    # Launch RViz for visualization and GUI-based motion planning and execution
    roslaunch panda_moveit_config moveit_rviz.launch

For more details, documentation and tutorials, please have a look at the
`MoveIt! tutorials website <http://docs.ros.org/kinetic/api/moveit_tutorials/html/>`_.

panda_python
--------------------
This package is developed to read from the text file containing the joint values and pass generate the trajectory using MoveIt.
The main parts of this code are:

Since joint values are recorded periodically, there are repeated values in the text files. To ignore repeated values this code block is used:

.. code-block:: python

   joint_val_sent = []
   prev_val = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
   for k in range(len(joint_val_recorded)):
     diff = False
     for l in range(len(joint_val_recorded[k])):
        if abs(prev_val[l] - joint_val_recorded[k][l]) > 0.1:
            diff = True
     if diff:
        prev_val = joint_val_recorded[k]
        joint_val_sent.append(joint_val_recorded[k])

For commanding the robot to move, the following code is used:

.. code-block:: python

   group = moveit_commander.MoveGroupCommander("panda_arm_hand")
   for joint_val in joint_val_sent:
    if not gripper_stat:
        joint_val.append(0.03)
        joint_val.append(0.03)
    else:
        joint_val.append(0.001)
        joint_val.append(0.001)
    group_variable_values = joint_val
    group.set_joint_value_target(group_variable_values)
    plan = group.plan()
    group.execute(plan)
    stat = get_finger_state(count, group_variable_values)

    if stat[0]:
        rospy.sleep(10)
        group_variable_values = stat[1]
        group.set_joint_value_target(group_variable_values)
        plan_grip = group.plan()
        group.execute(plan_grip)
        gripper_stat = True
    rospy.sleep(10)
    count = count + 1

here some example way-points were used for pick & place operation by defining variable "gripper_stat". For complete package visit `GitLab <https://gitlab.com/palashTUT/franka_lfd>`_
